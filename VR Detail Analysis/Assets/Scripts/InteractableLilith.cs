﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableLilith : Interactable {

    public Animator uiAnimator;

    public override void Go() {
        uiAnimator.SetTrigger("FadeIn");
    }

    public override void Stop() {
        uiAnimator.SetTrigger("FadeOut");
    }


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
