﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMovementController : MonoBehaviour {

    public NavMeshAgent playerAgent;
    public GameObject followPoint;

    void Start () {
	}

    void Update() {
        Vector3 endPosition = GvrPointerInputModule.CurrentRaycastResult.worldPosition;
        Debug.Log("Clicked.2");

        //if (GvrController.ClickButton && GvrPointerInputModule.CurrentRaycastResult.isValid || Input.GetButtonDown("Fire1")) {
        //    Debug.Log ("Clicked.");
        //    GetInteraction(endPosition);
        //}
        GetInteraction(endPosition);
    }

    private GameObject lastRayCast;

    void GetInteraction(Vector3 endPosition) {
        GameObject obj = GvrPointerInputModule.CurrentRaycastResult.gameObject;
        if (lastRayCast != obj) {
            if (lastRayCast != null) {
                if (lastRayCast.tag == "Interactable") {
                    lastRayCast.GetComponent<Interactable>().Stop();
                } else {
                    //Debug.Log("Non-interactable object clicked, at point: " + GvrPointerInputModule.CurrentRaycastResult.worldPosition);
                    //playerAgent.SetDestination(GvrPointerInputModule.CurrentRaycastResult.worldPosition);
                }
            }

            lastRayCast = obj;


            if (lastRayCast.tag == "Interactable") {
                lastRayCast.GetComponent<Interactable>().Go();
                Debug.Log("Interactable object clicked, at point: " + GvrPointerInputModule.CurrentRaycastResult.worldPosition);
            } else {
                //Debug.Log("Non-interactable object clicked, at point: " + GvrPointerInputModule.CurrentRaycastResult.worldPosition);
                //playerAgent.SetDestination(GvrPointerInputModule.CurrentRaycastResult.worldPosition);
            }
        }
    }
}
