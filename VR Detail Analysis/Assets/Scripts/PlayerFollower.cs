﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollower : MonoBehaviour {

    public GameObject followObject;
    private Vector3 offset;

    void Start () {
        offset = transform.position - followObject.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = followObject.transform.position + offset;
	}
}
