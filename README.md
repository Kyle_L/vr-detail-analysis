### What is this repository for? ###

* This repository is for the management and organization of the VR Detail Analysis for Miami University's CEC 101 Grand Challenge Project.
* Version: Finished.

### Developed using###
* Unity 2018, game engine.
* Autodesk Maya, modeling software.
* Adobe Mixamo, animation software.
* Adobe Photoshop, graphic design software.
* Google Cardboard SDK.
* Oculus SDK.
* Android SDK.

### Abstract ###
* To determine at what LOD (Level of detail) and animation a user is comfortable with �virtual people�.

### Method ###
* Utilizing a VR app built for the Google Cardboard, compare how comfortable a user is regarding the �virtual people� who represent a range of LODs and animations.

### Result ###
* Users are more comfortable with �virtual people� that have either have very little LOD or high LOD because they can clearly tell that the �virtual person� is not real or trying to imitate real life. Adding more LOD to the �virtual person� makes the user expect the �virtual person� to respond in a more lifelike manner, including moving. However, there was lurking variables that might have impacted the outcome of the results.

### Survey ###
* The survey used to collect data from the project.
* https://goo.gl/forms/kNQjZ77CQzo03Qg92

### Data ###
* Survey data
* https://docs.google.com/spreadsheets/d/1B4uAcYMsaZz0JmQkXTNoUn7ZcUYE2aaJxmVk0KNwPwM/edit?usp=sharing

## Pictures ##
* Pictures are available here.
* https://drive.google.com/drive/folders/16HXHhu2U5CJJhK2H5AZ4AXnnvAERatYK?usp=sharing